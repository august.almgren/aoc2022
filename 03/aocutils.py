def loadinput():
	return open("input.txt").read().splitlines()

# iterator for splitting a list into equally sized chunks (last chunk may be smaller)
class chunk:
	def __init__(self, data, chunksize):
		self.data = data
		self.chunksize = chunksize

	def __iter__(self):
		self.i = 0
		return self

	def __next__(self):
		if self.i < len(self.data):
			nextchunk = self.data[self.i:self.i + self.chunksize]
			self.i += self.chunksize
			return nextchunk
		else:
			raise StopIteration

def split(data, n):
	return chunk(data, int(len(data) / n))
