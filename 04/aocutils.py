# note - this file is hard linked locally (not supported in git)

import functools

def loadinput():
	return open("input.txt").read().splitlines()

# iterator for splitting a list into equally sized chunks (last chunk may be smaller)
class chunk:
	def __init__(self, data, chunksize):
		self.data = data
		self.chunksize = chunksize
		self.len = len(data)

	def __iter__(self):
		self.i = 0
		return self

	def __next__(self):
		if self.i < self.len:
			nextchunk = self.data[self.i:self.i + self.chunksize]
			self.i += self.chunksize
			return nextchunk
		else:
			raise StopIteration

# iterator for splitting a list into n identically sized chunks (last chunk may be smaller)
def split(data, n):
	return chunk(data, int(len(data) / n))

# takes a list of lists, returns the values common to all lists as a set
def intersection(data):
	return functools.reduce(lambda a, b: set(a).intersection(set(b)), data)

# takes a list of lists, returns a single value common to all lists
def commonvalue(data):
	return list(intersection(data))[0]
